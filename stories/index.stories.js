import * as React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
// import { linkTo } from '@storybook/addon-links';

// import { Button, Welcome } from '@storybook/react/demo';

// import {} from '../src/assets/sass/partials/components/_SubscriptionManagement';
require('../src/assets/sass/partials/components/_SubscriptionManagement');

import { Welcome } from '../src/welcome';

storiesOf('Welcome', module).add('to Storybook', () => (
    <div className="welcome-to-storybook">
      <span>
        Welcome to <strong>Storybook</strong>
      </span>
      <Welcome />
    </div>
  )
);

import {ListItem} from '../src/war/js/view/react_components/subscription_management_hoc/components/ListItem';
storiesOf('LI', module).add('LI', ()=> (
  <ListItem 
    id={1}
    key={1}
    email={'mmmmmm'}
    startDelete={()=>action('startDelete')}
    startEdit={()=> action('startEdit')} 
  />
  )
);