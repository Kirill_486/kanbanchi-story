import * as React from 'react';

export const Welcome = () => (
    <div className="welcome-to-storybook">
        <span>
          Welcome to <strong>Storybook</strong>
        </span>
    </div>
)
